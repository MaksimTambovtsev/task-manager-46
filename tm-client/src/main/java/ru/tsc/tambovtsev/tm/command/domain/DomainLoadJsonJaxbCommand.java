package ru.tsc.tambovtsev.tm.command.domain;

import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.Domain;
import ru.tsc.tambovtsev.tm.dto.request.DataJsonLoadJaxBRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DomainLoadJsonJaxbCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "load-json-jaxb";

    @NotNull
    private final static String DESCRIPTION = "Load projects, tasks and users from json file";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().loadDataJsonJaxb(new DataJsonLoadJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
